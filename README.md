sudo apt-get install python3 python3-pip python3-dev
git clone https://github.com/tgalal/yowsup.git
(se supone el numero de telefono esta activado/registrado para usar whatsapp) ~.config/yowsup/59898355724/...
cd yowsup
python3 setup.py install

cd ..
git clone https://github.com/libretime/libretime.git
cd libretime
sudo apt install vagrant vagrant-libvirt libvirt-daemon-system vagrant-mutate libvirt-dev
sudo usermod -a -G libvirt $USER

sudo shutdown -r now

vagrant plugin install vagrant-libvirt  #cuidado con firewall
vagrant box add bento/debian-10 --provider=virtualbox
vagrant mutate debian-buster libvirt
vagrant up debian-buster --provider=libvirt
#Once you reach the web setup GUI you can click through it using the default values.#
#To connect to the vagrant machine you can run vagrant ssh ubuntu-xenial in the libretime directory.#


#If you have set up LibreTime so that it can be accessed from other computers, you would use a domain name instead. For example:

https://musicine.tk/radio
#You can log in for the first time with the user name admin and the password admin. #

#or from another machine, using the domain name of the Icecast server:
http://musicine.tk:8000/stream
